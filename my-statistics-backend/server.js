const express = require('express');
const cors = require('cors');
const fs = require('fs');
const Papa = require('papaparse');
const app = express();

app.use(cors({ 
  origin: 'http://localhost:3000',
  credentials: true 
}));

const readCSV = async (filePath) => {
  const csvFile = fs.readFileSync(filePath, 'utf8');
  return new Promise(resolve => {
    Papa.parse(csvFile, {
      header: true,
      complete: results => {
        resolve(results.data);
      }
    });
  });
};

const calculateRegionalStats = (data, key, statFunction) => {
  return data.reduce((acc, curr) => {
    const region = curr.nom_region;
    const year = curr.annee_publication;
    const value = parseFloat(curr[key]);

    if (!region || !year || isNaN(value)) {
      return acc;
    }

    if (!acc[region]) {
      acc[region] = {};
    }

    if (!acc[region][year]) {
      acc[region][year] = {
        total: 0,
        count: 0,
        average: 0,
      };
    }

    acc[region][year] = statFunction(acc[region][year], value);
    return acc;
  }, {});
};


const averageStats = (regionStat, value) => {
  regionStat.total += value;
  regionStat.count += 1;
  regionStat.average = regionStat.total / regionStat.count;
  return regionStat;
};

const sumStats = (regionStat, value) => {
  regionStat.total += value;
  regionStat.count += 1;
  return regionStat;
};


app.get('/', (req, res) => {
  res.send('Hello World!');
});

app.get('/data', async (req, res) => {
  try {
    let data = await readCSV('logements-et-logements-sociaux-dans-les-departements.csv');
    
    // Filtrage par année
    if (req.query.annee) {
      data = data.filter(row => row.annee_publication === req.query.annee);
    }

    // Filtrage par population de moins de 20 ans
    if (req.query.populationMoinsVingt) {
      const populationMoinsVingt = parseFloat(req.query.populationMoinsVingt);
      data = data.filter(row => parseFloat(row.population_de_moins_de_20_ans) >= populationMoinsVingt);
    }

    // Filtrage par taux de chômage (exemple : chomage=<5 pour taux inférieur à 5%)
    if (req.query.chomage) {
      const tauxChomage = parseFloat(req.query.chomage);
      data = data.filter(row => parseFloat(row.taux_de_chomage_au_t4_en) < tauxChomage);
    }

    // Filtrage par nombre de logements
    if (req.query.nbLogements) {
      const nbLogements = parseInt(req.query.nbLogements, 10);
      data = data.filter(row => parseInt(row.nombre_de_logements) >= nbLogements);
    }

    // Filtrage par taux de logements sociaux
    if (req.query.tauxLogementsSociaux) {
      const tauxLogementsSociaux = parseFloat(req.query.tauxLogementsSociaux);
      data = data.filter(row => parseFloat(row.taux_de_logements_sociaux_en) >= tauxLogementsSociaux);
    }

    res.json(data);
  } catch (error) {
    res.status(500).send('Erreur lors de la lecture du fichier CSV');
  }
});

app.get('/statistics/habitants', async (req, res) => {
  const data = await readCSV('logements-et-logements-sociaux-dans-les-departements.csv');
  const stats = calculateRegionalStats(data, 'nombre_d_habitants', sumStats);
  res.json(stats);
});

app.get('/statistics/chomage', async (req, res) => {
  const data = await readCSV('logements-et-logements-sociaux-dans-les-departements.csv');
  const stats = calculateRegionalStats(data, 'taux_de_chomage_au_t4_en', averageStats);
  res.json(stats);
});

app.get('/statistics/constructions', async (req, res) => {
  const data = await readCSV('logements-et-logements-sociaux-dans-les-departements.csv');
  const stats = calculateRegionalStats(data, 'construction', sumStats);
  res.json(stats);
});
  
const PORT = 3001;
app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
