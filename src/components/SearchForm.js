import React, { useState } from 'react';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';

const SearchForm = ({ onSearch }) => {
  const [searchParams, setSearchParams] = useState({
    annee: '',
    populationMoinsVingt: '',
    tauxChomage: '',
    nbLogements: '',
    tauxLogementsSociaux: ''
  });

  const handleChange = (e) => {
    setSearchParams({ ...searchParams, [e.target.name]: e.target.value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    onSearch(searchParams);
  };

  return (
    <Box component="form" onSubmit={handleSubmit} sx={{ display: 'flex', justifyContent: 'center', gap: 2, m: 2 }}>
      <TextField
        name="annee"
        label="Année"
        variant="outlined"
        value={searchParams.annee}
        onChange={handleChange}
      />
      <TextField
        name="populationMoinsVingt"
        label="- de 20 ans"
        variant="outlined"
        value={searchParams.populationMoinsVingt}
        onChange={handleChange}
      />
      <TextField
        name="tauxChomage"
        label="Taux de chômage"
        variant="outlined"
        value={searchParams.tauxChomage}
        onChange={handleChange}
      />
      <TextField
        name="nbLogements"
        label="Nombre de logements"
        variant="outlined"
        value={searchParams.nbLogements}
        onChange={handleChange}
      />
      <TextField
        name="tauxLogementsSociaux"
        label="Taux de logements sociaux"
        variant="outlined"
        value={searchParams.tauxLogementsSociaux}
        onChange={handleChange}
      />
      <Button variant="contained" color="primary" type="submit">
        Rechercher
      </Button>
    </Box>
  );
};

export default SearchForm;
