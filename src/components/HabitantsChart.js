import React, { useState, useEffect } from 'react';
import Chart from 'react-apexcharts';

const HabitantsChart = () => {
  const [data, setData] = useState({});
  const [years, setYears] = useState([]);
  const [series, setSeries] = useState([]);
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    fetch('http://localhost:3001/statistics/habitants')
      .then(response => response.json())
      .then(data => {
        // Transformation des données pour ApexCharts
        const years = Object.keys(data['AUVERGNE-RHONE-ALPES']);
        const series = years.map(year => ({
          name: year,
          data: Object.values(data).map(region => region[year].total),
        }));

        // Obtenez la liste des noms de régions à partir des données
        const regions = Object.keys(data);
        
        setYears(years);
        setSeries(series);
        setCategories(regions);
      })
      .catch(error => console.error('Erreur de récupération des données', error));
  }, []);

  const options = {
    chart: {
      type: 'area',
      stacked: true,
    },
    fill: {
      opacity: 0.9,
    },
    legend: {
      position: 'top',
      labels: {
        useSeriesColors: true, // Utilisez les couleurs des séries pour les légendes
      },
    },
    xaxis: {
      categories: categories, 
      labels: {
        rotateAlways: -45, 
      },
    },
    title: {
      text: 'Graphique des Habitants par Région', // Titre du graphique
      align: 'center', 
      style: {
        fontSize: '24px', 
        fontWeight: 'bold', 
      }
    },
  };

  const chartContainerStyle = {
    margin: '40px',
  };

  return (
    <div style={chartContainerStyle}>
      <Chart options={options} series={series} type="area" height={400} />
    </div>
  );
};

export default HabitantsChart;
