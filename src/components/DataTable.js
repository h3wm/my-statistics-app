import React from 'react';
import { MaterialReactTable } from 'material-react-table';

const DataTable = ({ data }) => {
  const columns = React.useMemo(
    () => [
      { accessorKey: 'annee_publication', header: 'Année de publication',  enableColumnFiltering: true },
      { accessorKey: 'code_departement', header: 'Code du département',  enableColumnFiltering: true },
      { accessorKey: 'nom_departement', header: 'Nom du département',  enableColumnFiltering: true },
      { accessorKey: 'code_region', header: 'Code de la région',  enableColumnFiltering: true },
      { accessorKey: 'nom_region', header: 'Nom de la région',  enableColumnFiltering: true },
      { accessorKey: 'nombre_d_habitants', header: 'Nombre d\'Habitants' },
      { accessorKey: 'densite_de_population_au_km2', header: 'Densité de Population' },
      { accessorKey: 'variation_de_la_population_sur_10_ans_en', header: 'Variation Population / 10 ans' },
      { accessorKey: 'dont_contribution_du_solde_naturel_en', header: 'Contribution Solde Naturel' },
      { accessorKey: 'dont_contribution_du_solde_migratoire_en', header: 'Contribution Solde Migratoire' },
      { accessorKey: 'population_de_moins_de_20_ans', header: 'Population < 20 ans (%)' },
      { accessorKey: 'population_de_60_ans_et_plus', header: 'Population ≥ 60 ans (%)' },
      { accessorKey: 'taux_de_chomage_au_t4_en', header: 'Taux de Chômage (%)' },
      { accessorKey: 'taux_de_pauvrete_en', header: 'Taux de Pauvreté (%)' },
      { accessorKey: 'nombre_de_logements', header: 'Nombre de Logements' },
      { accessorKey: 'nombre_de_residences_principales', header: 'Résidences Principales' },
      { accessorKey: 'taux_de_logements_sociaux_en', header: 'Taux Logements Sociaux (%)' },
      { accessorKey: 'taux_de_logements_vacants_en', header: 'Taux Logements Vacants (%)' },
      { accessorKey: 'taux_de_logements_individuels_en', header: 'Taux Logements Individuels (%)' },
      { accessorKey: 'moyenne_annuelle_de_la_construction_neuve_sur_10_ans', header: 'Construction Neuve Annuelle' },
      { accessorKey: 'construction', header: 'Construction' },
      { accessorKey: 'parc_social_nombre_de_logements', header: 'Parc Social Logements' },
      { accessorKey: 'parc_social_logements_mis_en_location', header: 'Parc Social Logements Location' },
      { accessorKey: 'parc_social_logements_demolis', header: 'Parc Social Logements Démolis' },
      { accessorKey: 'parc_social_ventes_a_des_personnes_physiques', header: 'Ventes à Personnes Physiques' },
      { accessorKey: 'parc_social_taux_de_logements_vacants_en', header: 'Taux Logements Vacants (Parc Social)' },
      { accessorKey: 'parc_social_taux_de_logements_individuels_en', header: 'Taux Logements Individuels (Parc Social)' },
      { accessorKey: 'parc_social_loyer_moyen_en_eur_m2_mois', header: 'Loyer Moyen (€/m²/mois)'},
      { accessorKey: 'parc_social_age_moyen_du_parc_en_annees', header: 'Âge Moyen du Parc (années)' },
      { accessorKey: 'parc_social_taux_de_logements_energivores_e_f_g_en', header: 'Taux Logements Énergivores' }
    ],
    []
  );

  const containerStyle = {
    margin: '40px', 
  };

  return (
    <div style={containerStyle}>
      <MaterialReactTable
        columns={columns}
        data={data}
        initialState={{ density: 'compact' }} 
      />
    </div>
  );
};
export default DataTable;
