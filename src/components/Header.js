import React from 'react';

const Header = () => {
  return (
    <header>
      <h1>Statistiques des Logements</h1>
    </header>
  );
};

export default Header;
