import React, { useState, useEffect } from 'react';
import Chart from 'react-apexcharts';

const ChomageChart = () => {
  const [data, setData] = useState({});
  const [years, setYears] = useState([]);
  const [series, setSeries] = useState([]);
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    fetch('http://localhost:3001/statistics/chomage')
      .then(response => response.json())
      .then(data => {
        // Transformation des données pour ApexCharts
        const years = Object.keys(data['AUVERGNE-RHONE-ALPES']);
        const series = years.map(year => ({
          name: year,
          data: Object.values(data).map(region => parseFloat(region[year].average.toFixed(1))),
        }));

        // Obtenez la liste des noms de régions à partir des données
        const regions = Object.keys(data);

        setYears(years);
        setSeries(series);
        setCategories(regions);
      })
      .catch(error => console.error('Erreur de récupération des données', error));
  }, []);

  const options = {
    chart: {
      type: 'bar',
      stacked: true,
    },
    plotOptions: {
      bar: {
        horizontal: false,
      },
    },
    fill: {
      opacity: 0.9,
    },
    xaxis: {
      categories: categories, // Utilisez les noms des régions comme catégories
      labels: {
        rotateAlways: -45, // Rotation de -45 degrés pour les étiquettes en diagonale
      },
    },
    yaxis: {
      show: false, // Masquer l'axe Y
    },
    legend: {
      position: 'top',
    },
    dataLabels: {
      formatter: function (value) {
        return value.toFixed(1); // Arrondir les chiffres à 1 décimale
      },
    },
    title: {
      text: 'Taux de Chômage (2018 - 2022) en Pourcentage',
      align: 'center',
      style: {
        fontSize: '24px', 
        fontWeight: 'bold', 
      }
    },
  };

  const chartContainerStyle = {
    margin: '40px',
  };

  return (
    <div style={chartContainerStyle}>
      <Chart options={options} series={series} type="bar" height={400} />
    </div>
  );
};

export default ChomageChart;
