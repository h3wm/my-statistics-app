import React, { useState, useEffect } from 'react';
import Chart from 'react-apexcharts';

const ConstructionsChart = () => {
  const [data, setData] = useState({});
  const [years, setYears] = useState([]);
  const [series, setSeries] = useState([]);
  const [categories, setCategories] = useState([]);

  useEffect(() => {
    fetch('http://localhost:3001/statistics/constructions')
      .then(response => response.json())
      .then(data => {
        // Transformation des données pour ApexCharts
        const years = Object.keys(data['AUVERGNE-RHONE-ALPES']);
        const series = years.map(year => ({
          name: year,
          data: Object.values(data).map(region => region[year].total),
        }));

        // Obtenez la liste des noms de régions à partir des données
        const regions = Object.keys(data);

        setYears(years);
        setSeries(series);
        setCategories(regions);
      })
      .catch(error => console.error('Erreur de récupération des données', error));
  }, []);

  const options = {
    chart: {
      type: 'line',
    },
    xaxis: {
      categories: categories, // Utilisez les noms des régions comme catégories
      labels: {
        rotateAlways: -45, // Rotation de -45 degrés pour les étiquettes en diagonale
      },
    },
    yaxis: {
      show: false, // Masquer l'axe Y
    },
    title: {
      text: 'Nombre de constructions',
      align: 'center',
      style: {
        fontSize: '24px', 
        fontWeight: 'bold', 
      }
    },
    legend: {
      position: 'top',
      labels: {
        useSeriesColors: true, // Utilisez les couleurs des séries pour les légendes
      },
    },
  };

  // Style pour définir la largeur du conteneur du graphique en fonction de la largeur de l'écran
  const chartContainerStyle = {
    margin: '40px',
  };

  return (
    <div style={chartContainerStyle}>
      <Chart options={options} series={series} type="line" height={400} />
    </div>
  );
};

export default ConstructionsChart;
