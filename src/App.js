import React, { useState } from 'react';
import Header from './components/Header';
import SearchForm from './components/SearchForm';
import DataTable from './components/DataTable';
import HabitantsChart from './components/HabitantsChart';
import ChomageChart from './components/ChomageChart';
import ConstructionsChart from './components/ConstructionChart';

function App() {
  const [data, setData] = useState([]); // État pour stocker les données récupérées

  const handleSearch = async (searchParams) => {
    console.log('Recherche effectuée avec', searchParams);
    try {
      const queryParams = new URLSearchParams({
        annee: searchParams.annee,
        populationMoinsVingt: searchParams.populationMoinsVingt,
        chomage: searchParams.tauxChomage,
        nbLogements: searchParams.nbLogements,
        tauxLogementsSociaux: searchParams.tauxLogementsSociaux
      }).toString();
  
      const response = await fetch(`http://localhost:3001/data?${queryParams}`);
      const result = await response.json();
      console.log(result);
      setData(result); 
    } catch (error) {
      console.error('Erreur lors de la récupération des données:', error);
    }
  };
  
  return (
    <div className="App">
      <HabitantsChart />
      <ChomageChart />
      <ConstructionsChart />  
      <SearchForm onSearch={handleSearch} />  
      <DataTable data={data} />            
    </div>
  );
}

export default App;
