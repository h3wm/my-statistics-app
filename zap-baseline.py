#!/usr/bin/env python

import subprocess
import sys

# Check if the correct number of arguments is provided
if len(sys.argv) != 3:
    print("Usage: script.py http://localhost:3001/ report.html")
    sys.exit(1)

# The URL of the web application you want to analyze
target_url = sys.argv[1]

# The name of the output report (HTML)
report_name = sys.argv[2]

# Command to execute the security scan with ZAP
cmd = [
    "zap-baseline.py",
    "-t", target_url,  # Target URL
    "-r", report_name,  # Output report name (HTML)
]

# Execute the command
subprocess.run(cmd)
